
package notepad2;
    import java.awt.BorderLayout;
    import javax.swing.ImageIcon;
    import javax.swing.JLabel;
    import javax.swing.JWindow;
public class SplashScreen extends JWindow{
    private static final long serialVersionUID = -426853606813262298L;

    /**
     * Membuat SplashScreen baru, yang dapat menampilkan gambar dalam waktu
     * tertentu
     *
     * @param milisecond
     *           Waktu (dalam milisecond) tampilan gambar
     */
    public SplashScreen(long milisecond) {

        /* Menampilkan gambar pada saat pertama kali dijalankan */
        /* GetClass untuk mendapatkan kelas dari nama kelas.
           Kelas ini dapat digunakan sebagai parameter untuk rutinitas yang memerlukan kelas. */
        /* ImageIcon : Mengambil gambar icon dengan menggunakan fasilitas dari sebuah kelas.
           ImageIcon sendiri adalah sebuah kelas, menggunakan getClass() untuk menggunakan
           kelas getClassLoader dan menggunakan method getResource untuk masuk ke URL yg merujuk
           kepada gambar tertentu*/
        
         // Menampilkan gambar
         JLabel label = new JLabel(new ImageIcon(getClass().getClassLoader().getResource(
               "image/Usu.jpg")));
        
        /* Mengatur tata letak */
        
        getContentPane().setLayout(new BorderLayout());
        /* Mengatur possisi di tengah */
        
        getContentPane().add(label, BorderLayout.CENTER);
        

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
       

        try {
            /* Mematikan thread dalam mili detik */
            Thread.sleep(milisecond);
            /* Akan ditangani jika terjadi interupt */
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch(Throwable e){
            System.out.println("Kesalahan"+e);
        }finally {
            setVisible(false);
        }

    }


}
