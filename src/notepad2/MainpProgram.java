
package notepad2;
import javax.swing.SwingUtilities;
public class MainpProgram {
    public static void main(String[] args) {
        new SplashScreen(5000);
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                /* Membuat Objek frame, merujuk pada Kelas FrameUtama */
                FrameUtama frame = new FrameUtama();
                /* Menempatkan JFrame di tengah-tengan layar monitor */
                frame.setLocationRelativeTo(null);
                /* Membuat Frame bisa ditampilkan ke layar */
                frame.setVisible(true);
            }
        });
     }
}
