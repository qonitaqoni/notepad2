
package notepad2;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.DefaultEditorKit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

import notepad.dialogform.DialogContainer;
import notepad.dialogform.PanelAbout;
import notepad.dialogform.PanelFont;
import notepad.dialogform.PanelTextArea;
import notepad.exception.BadPanelException;
import notepad.utility.ArrayTextAction;
import notepad.utility.IOUtility;
public class FrameUtama extends javax.swing.JFrame{
    private static final long serialVersionUID = 112499409108089938L;
    private JMenu menu_file;
    private JMenuItem item_open;
    private JTextArea text_area;
    private JMenuItem item_about;
    private JMenu item_help;
    private JMenuItem item_area;
    private JMenuItem item_font;
    private JMenu menu_format;
    private JMenuItem item_exit;
    private JMenuItem item_selectall;
    private JMenuItem item_save;
    private JMenuItem item_paste;
    private JMenuItem item_cut;
    private JMenuItem item_copy;
    private JMenuItem item_redo;
    private JMenuItem item_undo;
    private JMenuItem item_saveas;
    private JMenu menu_edit;
    private JMenuBar menubar;
    private DialogContainer dialog;
    private PanelFont font;
    private ArrayTextAction aksi;
    private JMenuItem item_new;
    private UndoManager manager;
    private File currentFile = null;
    private PanelTextArea areasetting;
    private PanelAbout about;

    /**
     * Konstruktor class FrameUtama
     */
    public FrameUtama() {
        super();
        initGUI();
    }

    /**
     * Metode ini berisikan semua pendeklarasian komponen SWING
     */
    private void initGUI() {
        try {
            /* Pemanggilan sebuah gambar */
            this.setIconImage(new ImageIcon(getClass().getClassLoader().getResource(
                    "image/app.png")).getImage());
            /* Berfungsi untuk menutup aplikasi */
            this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
            /* Memberi Judul Pada Aplikasi */
            this.setTitle("Swing NotePad");
            {
                final JScrollPane jScrollPane1 = new JScrollPane();
                getContentPane().add(jScrollPane1, BorderLayout.CENTER);
                {
                    text_area = new JTextArea();
                    jScrollPane1.setViewportView(text_area);
                    {
                        aksi = new ArrayTextAction(text_area);
                    }
                    {
                        manager = new UndoManager();
                        text_area.getDocument().addUndoableEditListener(new UndoableEditListener() {

                            public void undoableEditHappened(UndoableEditEvent e) {
                                manager.addEdit(e.getEdit());
                                item_undo.setEnabled(manager.canUndo());
                                item_redo.setEnabled(manager.canRedo());
                            }

                        });
                    }
                    {
                        new DropTarget(text_area, new DropTargetListener() {

                            public void dragEnter(DropTargetDragEvent dtde) {
                                if(!dtde.isDataFlavorSupported(DataFlavor.stringFlavor)){
                                    dtde.rejectDrag();
                                }
                            }

                            public void dragExit(DropTargetEvent dte) {
                                // Do nothing
                            }

                            public void dragOver(DropTargetDragEvent dtde) {
                                if(!dtde.isDataFlavorSupported(DataFlavor.stringFlavor)){
                                    dtde.rejectDrag();
                                }
                            }

                            public void drop(DropTargetDropEvent dtde) {

                                dtde.acceptDrop(DnDConstants.ACTION_COPY);

                                Transferable transfer = dtde.getTransferable();
                                DataFlavor[] flavor = transfer.getTransferDataFlavors();

                                for(DataFlavor i : flavor){
                                    if(i.equals(DataFlavor.stringFlavor)){
                                        try {
                                            String str = (String) transfer.getTransferData(i);
                                            text_area.replaceSelection(str);
                                        } catch (UnsupportedFlavorException ex) {
                                            ex.printStackTrace();
                                        } catch (IOException ex) {
                                            ex.printStackTrace();
                                        }
                                    }
                                }

                                dtde.dropComplete(true);

                            }

                            public void dropActionChanged(DropTargetDragEvent dtde) {
                                if(!dtde.isDataFlavorSupported(DataFlavor.stringFlavor)){
                                    dtde.rejectDrag();
                                }
                            }
                        });
                    }
                }
            }
            {
                /* Membuat Objek baru, Merujuk pada JMenuBar */
                menubar = new JMenuBar();
                setJMenuBar(menubar);
                {
                    /* Membuat Objek baru [Menu File], Merujuk pada JMenu */
                    menu_file = new JMenu();
                    menubar.add(menu_file);
                    menu_file.setText("File");
                    menu_file.setMnemonic('F');
                    {
                        /* Membuat Objek baru [Menu Item], Merujuk pada JMenuItem */
                        item_new = new JMenuItem();
                        menu_file.add(item_new);
                        /* Memberi nama pada menu */
                        item_new.setText("New");
                        /* Memberikan control dari keyboard */
                        item_new.setAccelerator(KeyStroke.getKeyStroke("ctrl pressed N"));
                        /* Berfungsi untuk memberikan fungsi pada keyboard, ditandai dengan Underline */
                        item_new.setMnemonic('N');
                        /* Memanggil gambar */
                        item_new.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                                "image/new.png")));
                        /* Mengisi perintah pada menu */
                        item_new.addActionListener(new ActionListener() {

                            public void actionPerformed(ActionEvent e) {
                                IOUtility.newFile(FrameUtama.this, text_area, currentFile);
                                currentFile = null;
                                manager.discardAllEdits();
                            }

                        });
                    }
                    {
                        /* Membuat Objek baru [Menu Item], Merujuk pada JMenuItem */
                        item_open = new JMenuItem();
                        menu_file.add(item_open);
                        /* Memberi nama pada menu */
                        item_open.setText("Open");
                        item_open.setBounds(31, -3, 484, 20);
                        /* Memberikan control dari keyboard */
                        item_open.setAccelerator(KeyStroke.getKeyStroke("ctrl pressed O"));
                        /* Berfungsi untuk memberikan fungsi pada keyboard, ditandai dengan Underline */
                        item_open.setMnemonic('O');
                        /* Memanggil gambar */
                        item_open.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                                "image/open.png")));
                        /* Mengisi perintah pada menu */
                        item_open.addActionListener(new ActionListener() {

                            public void actionPerformed(ActionEvent e) {
                                File temp = currentFile;
                                String text = text_area.getText();
                                currentFile = IOUtility.openFile(FrameUtama.this, text_area, currentFile);
                                if (temp == currentFile) {
                                    text_area.setText(text);
                                }else{
                                    manager.discardAllEdits();
                                }
                            }

                        });
                    }
                    {
                        /* Membuat Objek baru [Menu Item], Merujuk pada JMenuItem */
                        item_save = new JMenuItem();
                        menu_file.add(item_save);
                        /* Memberi nama pada menu */
                        item_save.setText("Save");
                        /* Memberikan control dari keyboard */
                        item_save.setAccelerator(KeyStroke.getKeyStroke("ctrl pressed S"));
                        /* Berfungsi untuk memberikan fungsi pada keyboard, ditandai dengan Underline */
                        item_save.setMnemonic('S');
                        /* Memanggil gambar */
                        item_save.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                                "image/save1.png")));
                        /* Mengisi perintah pada menu */
                        item_save.addActionListener(new ActionListener() {

                            public void actionPerformed(ActionEvent e) {
                                if (currentFile == null) {
                                    currentFile = IOUtility.saveFile(FrameUtama.this, text_area);
                                } else {
                                    IOUtility.saveNow(text_area, currentFile);
                                }
                            }

                        });
                    }
                    {
                        /* Membuat Objek baru [Menu Item], Merujuk pada JMenuItem */
                        item_saveas = new JMenuItem();
                        menu_file.add(item_saveas);
                        /* Memberi nama pada menu */
                        item_saveas.setText("Save As");
                        /* Memberikan control dari keyboard */
                        item_saveas.setAccelerator(KeyStroke.getKeyStroke("shift ctrl pressed S"));
                         /* Berfungsi untuk memberikan fungsi pada keyboard, ditandai dengan Underline */
                        item_saveas.setMnemonic('A');
                        /* Memanggil gambar */
                        item_saveas.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                                "image/save2.png")));
                        /* Mengisi perintah pada menu */
                        item_saveas.addActionListener(new ActionListener() {

                            public void actionPerformed(ActionEvent e) {
                                currentFile = IOUtility.saveFile(FrameUtama.this, text_area);
                            }

                        });
                    }
                    {
                        /* Membuat Objek baru [Menu Item], Merujuk pada JMenuItem */
                        item_exit = new JMenuItem();
                        menu_file.add(item_exit);
                        /* Memberi nama pada menu */
                        item_exit.setText("Exit");
                        /* Memberikan control dari keyboard */
                        item_exit.setAccelerator(KeyStroke.getKeyStroke("alt pressed F4"));
                        /* Berfungsi untuk memberikan fungsi pada keyboard, ditandai dengan Underline */
                        item_exit.setMnemonic('X');
                        /* Memanggil gambar */
                        item_exit.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                                "image/exit.png")));
                        /* Mengisi perintah pada menu */
                        item_exit.addActionListener(new ActionListener() {

                            public void actionPerformed(ActionEvent e) {
                                if (currentFile == null) {
                                    if (text_area.getDocument().getLength() > 0) {
                                        int sure = JOptionPane.showConfirmDialog(FrameUtama.this,
                                                "Do you want to save it ?");
                                        if (sure == JOptionPane.OK_OPTION) {
                                            IOUtility.saveFile(FrameUtama.this, text_area);
                                            System.exit(0);
                                        } else {
                                            System.exit(0);
                                        }
                                    } else {
                                        System.exit(0);
                                    }
                                } else {
                                    int sure = JOptionPane.showConfirmDialog(FrameUtama.this,
                                            "Do you want to save it?");
                                    if (sure == JOptionPane.OK_OPTION) {
                                        IOUtility.saveNow(text_area, currentFile);
                                        System.exit(0);
                                    } else {
                                        System.exit(0);
                                    }
                                }
                            }

                        });
                    }
                }
                {
                    /* Membuat Objek baru, Merujuk pada JMenu */
                    menu_edit = new JMenu();
                    menubar.add(menu_edit);
                    menu_edit.setText("Edit");
                    menu_edit.setMnemonic('E');
                    {
                        /* Membuat Objek baru [Menu Item], Merujuk pada JMenuItem */
                        item_undo = new JMenuItem();
                        menu_edit.add(item_undo);
                        /* Memberi nama pada menu */
                        item_undo.setText("Undo");
                        /* Memberikan control dari keyboard */
                        item_undo.setAccelerator(KeyStroke.getKeyStroke("ctrl pressed Z"));
                        /* Berfungsi untuk memberikan fungsi pada keyboard, ditandai dengan Underline */
                        item_undo.setMnemonic('U');
                        item_undo.setEnabled(false);
                        /* Memanggil gambar */
                        item_undo.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                                "image/undo.png")));
                        /* Mengisi perintah pada menu */
                        item_undo.addActionListener(new ActionListener() {

                            public void actionPerformed(ActionEvent e) {
                                try {
                                    manager.undo();
                                } catch (CannotUndoException t) {
                                } finally {
                                    item_undo.setEnabled(manager.canUndo());
                                    item_redo.setEnabled(manager.canRedo());
                                }
                            }

                        });
                    }
                    {
                        /* Membuat Objek baru [Menu Item], Merujuk pada JMenuItem */
                        item_redo = new JMenuItem();
                        menu_edit.add(item_redo);
                        /* Memberi nama pada menu */
                        item_redo.setText("Redo");
                        /* Memberikan control dari keyboard */
                        item_redo.setAccelerator(KeyStroke.getKeyStroke("ctrl pressed Y"));
                        /* Berfungsi untuk memberikan fungsi pada keyboard, ditandai dengan Underline */
                        item_redo.setMnemonic('R');
                        item_redo.setEnabled(false);
                        /* Memanggil gambar */
                        item_redo.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                                "image/redo.png")));
                        /* Mengisi perintah pada menu */
                        item_redo.addActionListener(new ActionListener() {

                            public void actionPerformed(ActionEvent e) {
                                try {
                                    manager.redo();
                                } catch (CannotRedoException t) {
                                } finally {
                                    item_undo.setEnabled(manager.canUndo());
                                    item_redo.setEnabled(manager.canRedo());
                                }
                            }

                        });
                    }
                    // JseparatorBerfungsi untuk membuat garis pemisah antara menu item
                    {
                        final JSeparator jSeparator1 = new JSeparator();
                        menu_edit.add(jSeparator1);
                    }
                    {
                        /* Membuat Objek baru [Menu Item], Merujuk pada JMenuItem */
                        item_copy = new JMenuItem();
                        item_copy.setAction(aksi.getAction(DefaultEditorKit.copyAction));
                        menu_edit.add(item_copy);
                        /* Memberi nama pada menu */
                        item_copy.setText("Copy");
                        /* Memberikan control dari keyboard */
                        item_copy.setAccelerator(KeyStroke.getKeyStroke("ctrl pressed C"));
                        /* Berfungsi untuk memberikan fungsi pada keyboard, ditandai dengan Underline */
                        item_copy.setMnemonic('C');
                        /* Memanggil gambar */
                        item_copy.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                                "image/copy.png")));
                    }
                    {
                        /* Membuat Objek baru [Menu Item], Merujuk pada JMenuItem */
                        item_cut = new JMenuItem();
                        item_cut.addActionListener(aksi.getAction(DefaultEditorKit.cutAction));
                        menu_edit.add(item_cut);
                        /* Memberi nama pada menu */
                        item_cut.setText("Cut");
                        /* Memberikan control dari keyboard */
                        item_cut.setAccelerator(KeyStroke.getKeyStroke("ctrl pressed X"));
                        /* Berfungsi untuk memberikan fungsi pada keyboard, ditandai dengan Underline */
                        item_cut.setMnemonic('U');
                        /* Memanggil gambar */
                        item_cut.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                                "image/cut.png")));
                    }
                    {
                        /* Membuat Objek baru [Menu Item], Merujuk pada JMenuItem */
                        item_paste = new JMenuItem();
                        item_paste.addActionListener(aksi.getAction(DefaultEditorKit.pasteAction));
                        menu_edit.add(item_paste);
                        /* Memberi nama pada menu */
                        item_paste.setText("Paste");
                        /* Memberikan control dari keyboard */
                        item_paste.setAccelerator(KeyStroke.getKeyStroke("ctrl pressed V"));
                        /* Berfungsi untuk memberikan fungsi pada keyboard, ditandai dengan Underline */
                        item_paste.setMnemonic('P');
                        /* Memanggil gambar */
                        item_paste.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                                "image/paste.png")));
                    }
                    {
                        /* Memberi garis penghalang antara menu item satu dengan menu item yang lain */
                        final JSeparator jSeparator2 = new JSeparator();
                        menu_edit.add(jSeparator2);
                    }
                    {
                        /* Membuat Objek baru [Menu Item], Merujuk pada JMenuItem */
                        item_selectall = new JMenuItem();
                        item_selectall.setAction(aksi.getAction(DefaultEditorKit.selectAllAction));
                        menu_edit.add(item_selectall);
                        /* Memberi nama pada menu */
                        item_selectall.setText("Select All");
                        /* Memberikan control dari keyboard */
                        item_selectall.setAccelerator(KeyStroke.getKeyStroke("ctrl pressed A"));
                        /* Berfungsi untuk memberikan fungsi pada keyboard, ditandai dengan Underline */
                        item_selectall.setMnemonic('A');
                    }
                }
                {
                    /* Membuat Objek baru, Merujuk pada JMenu */
                    menu_format = new JMenu();
                    menubar.add(menu_format);
                    menu_format.setText("Format");
                    menu_format.setMnemonic('T');
                    {
                        /* Membuat Objek baru [Menu Item], Merujuk pada JMenuItem */
                        item_font = new JMenuItem();
                        menu_format.add(item_font);
                        /* Memberi nama pada menu */
                        item_font.setText("Font");
                        /* Memberikan control dari keyboard */
                        item_font.setAccelerator(KeyStroke.getKeyStroke("ctrl pressed F"));
                        /* Berfungsi untuk memberikan fungsi pada keyboard, ditandai dengan Underline */
                        item_font.setMnemonic('F');
                        item_font.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                                "image/font.png")));
                        item_font.addActionListener(new ActionListener() {

                            public void actionPerformed(ActionEvent e) {
                                showFontDialog();
                            }

                        });
                    }
                    //{
                        /* Membuat Objek baru [Menu Item], Merujuk pada JMenuItem */
                        //item_area = new JMenuItem();
                        //menu_format.add(item_area);
                        /* Memberi nama pada menu */
                        //item_area.setText("Text Area");
                        /* Memberikan control dari keyboard */
                        //item_area.setAccelerator(KeyStroke.getKeyStroke("ctrl pressed T"));
                        /* Berfungsi untuk memberikan fungsi pada keyboard, ditandai dengan Underline */
                        //item_area.setMnemonic('A');
                        /* Memanggil gambar */
                        //item_area.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                          //      "image/paragraf.png")));
                        /* Memberi perintah pada menu */
                        //item_area.addActionListener(new ActionListener() {

                            //public void actionPerformed(ActionEvent e) {
                           //     showAreaDialog();
                         //   }

                       // });
                    //}
                }
                {
                    Component glue = Box.createHorizontalGlue();
                    menubar.add(glue);
                }
                {
                    /* Membuat Objek baru, Merujuk pada JMenu */
                    item_help = new JMenu();
                    menubar.add(item_help);
                    item_help.setText("Help");
                    item_help.setMnemonic('H');
                    {
                        /* Membuat Objek baru [Menu Item], Merujuk pada JMenuItem */
                        item_about = new JMenuItem();
                        item_help.add(item_about);
                        /* Memberi nama pada menu */
                        item_about.setText("About");
                        /* Berfungsi untuk memberikan fungsi pada keyboard, ditandai dengan Underline */
                        item_about.setMnemonic('A');
                        /* Memberikan control dari keyboard */
                        item_about.setAccelerator(KeyStroke.getKeyStroke("shift ctrl alt pressed A"));
                        /* Memanggil gambar */
                        item_about.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                                "image/about.png")));
                        /* Memberi perintah pada menu */
                        item_about.addActionListener(new ActionListener() {

                            public void actionPerformed(ActionEvent e) {
                                showAboutDialog();
                            }

                        });
                    }
                }
            }
            {
                dialog = new DialogContainer(this);
                {
                    font = new PanelFont(text_area);
                }
                {
                    areasetting = new PanelTextArea(text_area);
                }
                {
                    about = new PanelAbout();
                }
            }
            pack();
            this.setSize(621, 469);
            this.addWindowListener(new WindowAdapter() {

                @Override
                public void windowClosing(WindowEvent e) {
                    if (currentFile == null) {
                        if (text_area.getDocument().getLength() > 0) {
                            int sure = JOptionPane.showConfirmDialog(FrameUtama.this,
                                    "Do you want to save it?");
                            if (sure == JOptionPane.OK_OPTION) {
                                IOUtility.saveFile(FrameUtama.this, text_area);
                                System.exit(0);
                            } else {
                                System.exit(0);
                            }
                        } else {
                            System.exit(0);
                        }
                    } else {
                        int sure = JOptionPane.showConfirmDialog(FrameUtama.this,
                                "Do you want to save it?");
                        if (sure == JOptionPane.OK_OPTION) {
                            IOUtility.saveNow(text_area, currentFile);
                            System.exit(0);
                        } else {
                            System.exit(0);
                        }
                    }
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showAboutDialog() {
        try {
            dialog.showDialog(about);
        } catch (BadPanelException e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE,
                    null);
        }
    }

    /**
     * Metode ini digunakan untuk menampilkan Area Dialog yang digunakan untuk
     * mengedit area text editor
     */
    private void showAreaDialog() {
        try {
            dialog.showDialog(areasetting);
        } catch (BadPanelException e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE,
                    null);
        }
    }

    /**
     * Metode ini digunakan untuk menampilkan Font Dialog, yang digunakan untuk
     * mengedit Font Text
     */
    private void showFontDialog() {
        try {
            dialog.showDialog(font);
        } catch (BadPanelException e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE,
                    null);
        }
    }
}
